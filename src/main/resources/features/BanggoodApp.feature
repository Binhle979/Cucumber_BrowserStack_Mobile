Feature:Test app Banggood

#  Background:
#    Given App open successfully

  Scenario Outline: Test case 1
    When User click to the Category on footer menu
    And Scroll and click to Home and Garden in Left menu
    And After the right category displayed, click to the “Home Decor”
    And Input price from <valueMin> to <valueMax> for filter
    And User click first product
    Then User should see product name displayed and product price in range <valueMin> to <valueMax>
    Examples:
      | valueMin | valueMax |
      | 20       | 30       |

  Scenario: Test case 2
    When User scroll to Hot Categories at Home screen and click to first category
    And User click first product
    And After product detail displayed , user click Add to cart
    And User click to Cart icon on header
    Then User should see product name, product size, product price and quantity

  Scenario: Test case 3
    When User click Account on footer menu
    And User click View All Order
    Then The login screen should be displayed with: Email, password and Sign In button

  Scenario: Test case 4
    Given User scroll to Group Buy button in home screen
    When User click to Group Buy button
    And User click to tab Hottest
    And User click to the first hot product
    Then User would see product name and product price

  Scenario: Test case 5
    Given User click to the search box and input the name of product
    |product|
    |iphone |
    When User press enter
    And User click to the first product displayed
    And After product detail displayed , user click Add to cart
    And User click to Cart icon on header
    Then User should see product name, product size, product price