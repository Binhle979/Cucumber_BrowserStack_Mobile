package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import keywords.AndroidKeywords;
import locators.Locators;

public class Scenario4Steps {
    AndroidKeywords keywords = new AndroidKeywords();
    Locators locators = new Locators();

    @Given("User scroll to Group Buy button in home screen")
    public void userScrollToGroupBuyButtonInHomeScreen() {
        keywords.scrollToElementDownByPercent(locators.groupBuy,2);
    }

    @When("User click to Group Buy button")
    public void userClickToGroupBuyButton() {
        keywords.clickElementByXpath(locators.groupBuy);
    }

    @And("User click to tab Hottest")
    public void userClickToTabHottest() {
        keywords.waitToClickAble(locators.tabHottest,10);
        keywords.clickElementByXpath(locators.tabHottest);
    }

    @And("User click to the first hot product")
    public void userClickToTheFirstHotProduct() {
        keywords.waitToClickAble(locators.firstHotProduct,10);
        keywords.clickElementByXpath(locators.firstHotProduct);
    }

    @Then("User would see product name and product price")
    public void userWouldSeeProductNameAndProductPrice() {
        keywords.verifyElementByXpath(locators.hotProductName);
        keywords.verifyElementByXpath(locators.hotProductPrice);
        keywords.goBack();
        keywords.goBack();
    }

    @When("User click to the first product displayed")
    public void userClickToTheFirstProductDisplayed() {
        keywords.waitToClickAble(locators.firstProduct,10);
        keywords.clickElementByXpath(locators.firstProduct);
    }
}
