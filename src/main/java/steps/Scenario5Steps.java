package steps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import keywords.AndroidKeywords;
import locators.Locators;
import org.testng.Assert;

import java.util.List;
import java.util.Map;

public class Scenario5Steps {
    AndroidKeywords keywords = new AndroidKeywords();
    Locators locators = new Locators();
    String productName;

    @Given("User click to the search box and input the name of product")
    public void userClickToTheSearchBoxAndInputTheNameOfProduct(DataTable dataTable) {
        List<Map<String,String>> data=dataTable.asMaps(String.class,String.class);
        productName = data.get(0).get("product");
        keywords.clickElementByXpath(locators.searchBox);
    }

    @When("User press enter")
    public void userPressEnter() {
        keywords.sendKeyByXpath(locators.searchBoxEdit,productName+"\\n");
    }

    @Then("User should see product name, product size, product price")
    public void userShouldSeeProductNameProductSizeProductPrice() {
        keywords.waitUntilPageContainsElement(locators.productName);
        keywords.verifyElementByXpath(locators.productName);
        keywords.verifyElementByXpath(locators.productPrice);
        keywords.closeApplication();
    }
}
