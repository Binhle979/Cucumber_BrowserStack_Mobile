import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        features = "src/main/resources/features/BanggoodApp.feature",
        plugin = { "pretty"}
)
public class runnerCucumber extends AbstractTestNGCucumberTests {
}
